<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'elizabeth_shaw' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ']RbjG>0&$*SO~8@[BUSB$k48,qg0fR_Y{wMS=quqc*N y}b`G>M&uI_ddxwOG4&w' );
define( 'SECURE_AUTH_KEY',  '5_ahAX,et!;mv5Tv$i0f[NZ06^I|#N?ZGj oS$`.Eqp9ay-&sK6mqx+*SeX]#8i3' );
define( 'LOGGED_IN_KEY',    '<9Gk:quM1D-rFMKeQVEQW<.hDn/v]^+IW uKypdIgd-k;$A#]vH/JcCN>n]<(p) ' );
define( 'NONCE_KEY',        'Z}!l?2p6@x~ZPp2:P.>W<?1Xq`+,]1JFo~/E35H((1!Fd~,MCPJLXx_QxYo^NG1m' );
define( 'AUTH_SALT',        '!_tWMzhj&sS`oDOt 8Q}=R|(c>_Zd.XIFwl77;J<VSA@sGI*pS/2^niY^3P-f6ET' );
define( 'SECURE_AUTH_SALT', '?p<_1d}QN3$:>{*6 )}E:)R_p!~3x|&{RbR~G[gm5J.Gy>!<[{$y<3D2}GJgQSrt' );
define( 'LOGGED_IN_SALT',   '0K7|AoH,rL0`%+YpKv rJ~e^8^cn%Q=KBHG)x5::ZcV~iKA5B? J8*hD$gi#QK q' );
define( 'NONCE_SALT',       ' M&Su+$l?H -$RX5>iYJlX.@U& 7 vlBp)9,4VbHup[=#.R4X!| f2ef*_8cC@-3' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
