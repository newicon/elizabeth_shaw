<?php
/**
 * Remove product data tabs
 */
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    return $tabs;
}

/**
 * Add a custom product data tab
 */
function woo_new_product_tab( $tabs ) {

    // Adds the new tab

    $tabs['delivery_tab'] = array(
        'title' 	=> __( 'Delivery Information', 'woocommerce' ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_new_delivery_tab_content'
    );

    // Adds the new tab

    $tabs['bundle_tab'] = array(
        'title' 	=> __( "What's in my bundle", 'woocommerce' ),
        'priority' 	=> 50,
        'callback' 	=> 'woo_inside_bundle_tab_content'
    );

    return $tabs;

}
function woo_new_delivery_tab_content() {
    // The new tab content
    echo '<p>To create a custom field in order to let the client insert the delivery information here</p>';
}
function woo_inside_bundle_tab_content() {
    // The new tab content
    echo '<p>Here the product that are in my bundle to display</p>';
}

/**
 * Reorder product data tabs
 */

function woo_reorder_tabs( $tabs ) {

    $tabs['bundle_tab']['priority'] = 10;
    $tabs['description']['priority'] = 15;
    $tabs['additional_information']['priority'] = 20;
    $tabs['delivery_tab']['priority'] = 25;

    return $tabs;
}