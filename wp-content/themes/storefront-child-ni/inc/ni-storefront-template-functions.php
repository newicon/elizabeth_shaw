<?php

if ( ! function_exists( 'ni_storefront_primary_navigation' ) ) {
    /**
     * Display Primary Navigation
     *
     * @since  1.0.0
     * @return void
     */
    function ni_storefront_primary_navigation() {
        ?>
        <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
            <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location'  => 'primary',
                    'container_class' => 'primary-navigation',
                )
            );

            wp_nav_menu(
                array(
                    'theme_location'  => 'handheld',
                    'container_class' => 'handheld-navigation',
                )
            );
            ?>
        </nav><!-- #site-navigation -->
        <?php
    }
}
if ( ! function_exists( 'ni_storefront_site_branding' ) ) {
    /**
     * Site branding wrapper and display
     *
     * @since  1.0.0
     * @return void
     */
    function ni_storefront_site_branding() {
        ?>
        <div class="site-branding">
            <?php ni_storefront_site_title_or_logo(); ?>
        </div>
        <div class="es_cart-search">
            <a href="" title=""><i class="fas fa-shopping-cart"></i></a>
            <div es_search>
                <a href="" title=""><i class="fas fa-search"></i></a>
                <div class="es_search_wrap"></div>
            </div>
        </div>
        <?php
    }
}
if ( ! function_exists( 'ni_storefront_site_title_or_logo' ) ) {
    /**
     * Display the site title or logo
     *
     * @since 2.1.0
     * @param bool $echo Echo the string or return it.
     * @return string
     */
    function ni_storefront_site_title_or_logo( $echo = true ) {
        if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
            $logo = get_custom_logo();
            $html = is_home() ? '<div class="logo">' . $logo . '</div>' : $logo;
        } else {
            $tag = is_home() ? 'h1' : 'div';

            $html = '<' . esc_attr( $tag ) . ' class="beta site-title"><a href="' . esc_url( home_url( '/' ) ) . '" rel="home">' . esc_html( get_bloginfo( 'name' ) ) . '</a></' . esc_attr( $tag ) . '>';

            if ( '' !== get_bloginfo( 'description' ) ) {
                $html .= '<p class="site-description">' . esc_html( get_bloginfo( 'description', 'display' ) ) . '</p>';
            }
        }

        if ( ! $echo ) {
            return $html;
        }

        echo $html; // WPCS: XSS ok.
    }
}
if ( ! function_exists( 'wc_remove_storefront_breadcrumbs' ) ) {
    /**
     * Remove Woocommerce Breadcrumbs
     * @param
     * @return
     *
     */
    function wc_remove_storefront_breadcrumbs() {
        remove_action( 'storefront_before_content', 'woocommerce_breadcrumb', 10 );
    }

}
/**
 * Remove Storefrot Sidebar
 * @param
 * @return
 *
 */
function remove_storefront_sidebar() {
    if ( is_woocommerce() ) {
        remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
    }
}



