<?php


require 'inc/ni-storefront-template-functions.php';
require 'inc/ni-woo-product-tab-functions.php';
require 'inc/ni-woo-custom-fields.php';



/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );






/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}



/**
 * Newicon Storefront HOOKS
 *
 */



/**
 * General
 *
 * @see  storefront_header_widget_region()
 * @see  ni_storefront_get_sidebar()
 */
remove_action( 'storefront_before_content', 'storefront_header_widget_region', 10 );
remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );


/**
 * Header
 *
 * @see ni-storefront-template-functions.php
 */
add_action( 'storefront_header', 'storefront_header_container', 0 );
add_action( 'ni_storefront_header', 'ni_storefront_site_branding', 5 );
add_action( 'ni_storefront_header', 'storefront_skip_links', 10 );
add_action( 'ni_storefront_header', 'storefront_header_container_close', 41 );
add_action( 'ni_storefront_header', 'storefront_primary_navigation_wrapper', 42 );
add_action( 'ni_storefront_header', 'ni_storefront_primary_navigation', 50 );
add_action( 'ni_storefront_header', 'storefront_primary_navigation_wrapper_close', 68 );
add_action( 'init', 'wc_remove_storefront_breadcrumbs');




/**
 * Find better solution
 * trying to remove storefront terrible sidebar
 */
add_action( 'get_header', 'remove_storefront_sidebar' );





/**
 * Customizing woocommerce tab
 * @see ni-woo-product-tab-function.php
 */
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );





















